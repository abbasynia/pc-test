import { getTotalJourneysByMode, getCostPerKilometerByMode, getAverageSpeedByMode } from "./dataAnalysis"
import { mockSurveyResults } from "../mockData"


describe('getTotalJourneysByMode', () => {
    let totalJourneys
    beforeEach(() => {
        totalJourneys = getTotalJourneysByMode(mockSurveyResults)
    })
    it('returns expected sum for Walk journeys', () => {
        expect(totalJourneys.Walk).toBe(1)
    })
    it('returns expected sum for Bike journeys', () => {
        expect(totalJourneys.Bike).toBe(2)
    })
    it('returns expected sum for Car journeys', () => {
        expect(totalJourneys.Car).toBe(2)
    })
    it('returns expected sum for Bus journeys', () => {
        expect(totalJourneys.Bus).toBe(2)
    })
    it('returns expected sum for Train journeys', () => {
        expect(totalJourneys.Train).toBe(3)
    })
    it('returns expected sum for Underground journeys', () => {
        expect(totalJourneys.Underground).toBe(3)
    })
})

describe('getCostPerKilometerByMode', () => {
    let costPerKilometerByMode
    beforeEach(() => {
        costPerKilometerByMode = getCostPerKilometerByMode(mockSurveyResults)
    })
    it('returns expected cost/km for Walk journeys', () => {
        expect(costPerKilometerByMode.Walk).toBe(0)
    })
    it('returns expected cost/km for Bike journeys', () => {
        expect(costPerKilometerByMode.Bike).toBe(0)
    })
    it('returns expected cost/km for Car journeys', () => {
        expect(costPerKilometerByMode.Car).toBe(0.9682539682539683)
    })
    it('returns expected cost/km for Bus journeys', () => {
        expect(costPerKilometerByMode.Bus).toBe(0.95)
    })
    it('returns expected cost/km for Train journeys', () => {
        expect(costPerKilometerByMode.Train).toBe(0.71875)
    })
    it('returns expected cost/km for Underground journeys', () => {
        expect(costPerKilometerByMode.Underground).toBe(0.775)
    })

})

describe('getAverageSpeedByMode', () => {
    let averageSpeedByMode
    beforeEach(() => {
        averageSpeedByMode = getAverageSpeedByMode(mockSurveyResults)
    })
    it('returns expected cost/km for Walk journeys', () => {
        expect(averageSpeedByMode.Walk).not.toBeUndefined()
    })
    it('returns expected cost/km for Bike journeys', () => {
        expect(averageSpeedByMode.Bike).not.toBeUndefined()
    })
    it('returns expected cost/km for Car journeys', () => {
        expect(averageSpeedByMode.Car).not.toBeUndefined()
    })
    it('returns expected cost/km for Bus journeys', () => {
        expect(averageSpeedByMode.Bus).not.toBeUndefined()
    })
    it('returns expected cost/km for Train journeys', () => {
        expect(averageSpeedByMode.Train).not.toBeUndefined()
    })
    it('returns expected cost/km for Underground journeys', () => {
        expect(averageSpeedByMode.Underground).not.toBeUndefined()
    })
})