import { Survey, MODE_OF_TRANSPORT, SurveyTotalsByMode, SurveyTotals, CostPerKilometerByMode } from "../types";

export function getTotalJourneysByMode(completedSurveys: Survey[]): Record<MODE_OF_TRANSPORT, number> {
    return completedSurveys.reduce((totalJourneysByMode:Record<string, number>, survey:Survey) => {
        totalJourneysByMode[survey.modeOfTransport] = (totalJourneysByMode[survey.modeOfTransport] || 0) + 1
        return totalJourneysByMode
    }, {})
}

export function getAverageSpeedByMode(completedSurveys: Survey[]):  Record<MODE_OF_TRANSPORT, number> {
    return {} as Record<MODE_OF_TRANSPORT, number>
}

export function getCostPerKilometerByMode(completedSurveys: Survey[]): CostPerKilometerByMode {

    let totalsByMode: SurveyTotalsByMode = sumByMode(completedSurveys)

    let costPerKilometerByMode: CostPerKilometerByMode = {} as CostPerKilometerByMode

    Object.values(MODE_OF_TRANSPORT).forEach((mode: string) => {
        costPerKilometerByMode[mode as MODE_OF_TRANSPORT] =
            totalsByMode[mode].cost / totalsByMode[mode].distance
    })

    return costPerKilometerByMode
}

function sumByMode(completedSurveys: Survey[]): SurveyTotalsByMode {

    let totalsByMode: SurveyTotalsByMode = {} as SurveyTotalsByMode

    Object.values(MODE_OF_TRANSPORT).forEach((mode: string) => {
        totalsByMode[mode] =
            { distance: 0, cost: 0, time: 0 } as SurveyTotals

    })

    return completedSurveys.reduce((totalByMode: any, survey: Survey) => {
        totalsByMode[survey.modeOfTransport] = {
            cost: totalsByMode[survey.modeOfTransport].cost + survey.cost,
            time: totalsByMode[survey.modeOfTransport].cost + survey.time,
            distance: totalsByMode[survey.modeOfTransport].cost + survey.distance,
        }
        return totalsByMode
    }, totalsByMode)
} 