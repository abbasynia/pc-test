import reducer, {initialState} from "./index";
import {AddSurveyResultAction, ShowResultsAction, ShowSurveyAction} from "./types";

describe('reducer', () => {
    const baseState = initialState
    describe('show survey action', () => {
        it('should turn showResults to false', function () {
            const state = {
                ...baseState,
                showResults: true
            }

            const action: ShowSurveyAction = {
                type: 'SHOW_SURVEY'
            }
            expect(reducer(state, action).showResults).toBe(false)
        });
    });
    describe('show results action', () => {
        it('should turn showResults to true', function () {
            const state = {
                ...baseState,
                showResults: false
            }

            const action: ShowResultsAction = {
                type: 'SHOW_RESULTS'
            }
            expect(reducer(state, action).showResults).toBe(true)
        });
    });
    describe('add survey result action', () => {
        it('should add survey result to end of completed survey array', function () {
            const state = {
                ...baseState,
            }

            const action: AddSurveyResultAction = {
                type: 'ADD_SURVEY_RESULT',
                payload: {
                    result: {
                        username: 'bob',
                        modeOfTransport: 'train',
                        distance: 123,
                        time: 1234,
                        cost: 6456
                    }
                }
            }
            const reducedState = reducer(state, action)
            expect(reducedState.completedSurveys[reducedState.completedSurveys.length - 1]).toBe(action.payload.result)
        });
    });
});