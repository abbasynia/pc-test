import {Survey} from "../types";

export interface ShowResultsAction {
    type: 'SHOW_RESULTS',
}

export interface ShowSurveyAction {
    type: 'SHOW_SURVEY',
}

export interface AddSurveyResultAction {
    type: 'ADD_SURVEY_RESULT',
    payload: {
        result: Survey
    }
}

export type Action = ShowResultsAction | ShowSurveyAction | AddSurveyResultAction