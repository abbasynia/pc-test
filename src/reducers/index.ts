import { SurveyAppState } from '../types'
import { mockSurveyResults } from '../mockData'
import {Action} from "./types";

export const initialState: SurveyAppState = {
    completedSurveys: mockSurveyResults,
    showResults: false
}

export default (state: SurveyAppState = initialState, action: Action): SurveyAppState => {
    switch (action.type) {
        case 'SHOW_RESULTS':
            return {
                ...state,
                showResults: true,
            }
        case 'SHOW_SURVEY':
            return {
                ...state,
                showResults: false,
            }
        case 'ADD_SURVEY_RESULT': {
            return {
                ...state,
                completedSurveys: [
                    ...state.completedSurveys,
                    action.payload.result,
                ]
            }
        }
        default:
            return state
    }
}