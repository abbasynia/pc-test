import {Survey} from "../types";

export const addSurveyResultAction = (result: Survey) => ({
    type: 'ADD_SURVEY_RESULT',
    payload: {
        result,
    }
})
