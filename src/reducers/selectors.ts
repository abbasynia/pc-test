import {SurveyAppState} from "../types";

export const getCompletedSurveys = (state: SurveyAppState) => state.completedSurveys