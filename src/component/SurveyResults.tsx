import React from 'react';
import { Survey, SurveyAppState } from '../types';
import { connect } from 'react-redux';
import CostPerKm from './stats/CostPerKm';
import './SurveyResults.css'
import NumberOfJourneys from './stats/NumberOfJourneys';
import AverageSpeed from "./stats/AverageSpeed";

interface SurveyResultsProps {
    completedSurveys: Survey[]
}

class SurveyResults extends React.Component<SurveyResultsProps> {

  render() {
      console.log(this.props)
      return (<div>
            <div className="test-comment">For each mode of transport we would like to display some statistics such as the percentage of journeys using that mode of transport, the average cost, average distance covered, average time taken, average speed, cost per kilometre etc. <div className="emph">Please code at least one of these panels, ensuring that your business logic is unit tested, and an enzyme test is provided for the new component</div></div>
            <div className="survey-stats">
                <CostPerKm/>
                <NumberOfJourneys/>
                <AverageSpeed />
                <div className="stats-panel"><div className="panel-title">Average Cost per journey</div><div className="test-comment">TBD!</div></div>
                <div className="stats-panel"><div className="panel-title">Average Distance covered</div><div className="test-comment">TBD!</div></div>
                <div className="stats-panel"><div className="panel-title">Average Time Taken</div><div className="test-comment">TBD!</div></div>
            </div>
      </div>)
  }
}

function mapStateToProps(state:SurveyAppState) {
    return {
        completedSurveys: state.completedSurveys
    }
}

export default connect(mapStateToProps)(SurveyResults)