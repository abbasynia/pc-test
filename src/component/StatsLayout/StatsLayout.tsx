import React from 'react'

type Props = {
    title: string,
    rows: {
        transport: string,
        value: string
    }[],
    additionalRows?: {
        key: string,
        value: string,
    }[]
}

export default class StatsLayout extends React.Component<Props, {}> {
    render() {
        return (
            <div>
                <div className='title'>{this.props.title}</div>
                {this.props.rows.map(r => <div key={r.transport} className='row'>{r.transport}: {r.value}</div>)}
                {this.props.additionalRows && this.props.additionalRows.map(r => <div key={r.key}>{r.key}: {r.value}</div>)}
            </div>
        )
    }
}