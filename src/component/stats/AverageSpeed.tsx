import {MODE_OF_TRANSPORT, Survey, SurveyAppState} from "../../types"
import React from 'react'
import {connect} from "react-redux"
import StatsLayout from "../StatsLayout/StatsLayout";
import {createSelector} from "reselect";
import {getCompletedSurveys} from "../../reducers/selectors";

type AverageSpeedProps = ReturnType<typeof mapStateToProps>

const AverageSpeed: React.FunctionComponent<AverageSpeedProps> = (props: AverageSpeedProps) => {
    return <StatsLayout title="Average Speed (km/h)" rows={props.averageSpeeds}/>
}

type SpeedAggregate = {
    [index: string]: {
        numberOfSpeeds: number,
        totalSpeed: number,
    }
}

// Use createSelector to memoise selectors for all components
// Also do data manipulation in mapStateToProps where possible See https://react-redux.js.org/using-react-redux/connect-mapstate#let-mapstatetoprops-reshape-the-data-from-the-store

const mapStateToProps = createSelector(getCompletedSurveys, completedSurveys => {
    const aggregate = completedSurveys.reduce<SpeedAggregate>((acc, survey: Survey) => {
            if (acc[survey.modeOfTransport]) {
                acc[survey.modeOfTransport] = {
                    numberOfSpeeds: acc[survey.modeOfTransport].numberOfSpeeds + 1,
                    totalSpeed: acc[survey.modeOfTransport].totalSpeed + (survey.distance / survey.time)
                }
            } else {
                acc[survey.modeOfTransport] = {
                    numberOfSpeeds: 1,
                    totalSpeed: (survey.distance / survey.time)
                }
            }
            return acc
        }, {})

    return {
        averageSpeeds: Object.keys(aggregate).map((mode: string) => ({
            transport: mode,
            value: (aggregate[mode as MODE_OF_TRANSPORT].totalSpeed / aggregate[mode as MODE_OF_TRANSPORT].numberOfSpeeds).toFixed(2)
        }))
    }
})

export default connect(mapStateToProps)(AverageSpeed);
