import { MODE_OF_TRANSPORT, SurveyAppState, Survey } from "../../types"
import { getTotalJourneysByMode } from "../../helpers/dataAnalysis"
import { connect } from "react-redux"
import React from 'react'

interface NumberOfJourneysProps {
    completedSurveys: Survey[]
}

export const NumberOfJourneys:React.FunctionComponent<NumberOfJourneysProps> = (props:NumberOfJourneysProps) => {
return (<div className="stats-panel">
    <div className="panel-title">Number of journeys</div>
    <div>Total: {props.completedSurveys.length}</div>
    {(Object.values(MODE_OF_TRANSPORT) as string[]).map((mode:string) => {
        return (<div key={mode} className='stats-row'><span className='transport-mode'>{mode}</span><span className="stats-value">{getTotalJourneysByMode(props.completedSurveys)[mode as MODE_OF_TRANSPORT]}</span></div>)
    } ) }
</div>)

}

function mapStateToProps(state:SurveyAppState) {
    return {
        completedSurveys: state.completedSurveys
    }
}

export default connect(mapStateToProps)(NumberOfJourneys);
