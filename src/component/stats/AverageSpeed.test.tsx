import React from 'react';
import {mount} from 'enzyme';
import { mockSurveyResults } from '../../mockData';
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducers from '../../reducers'
import AverageSpeed from "./AverageSpeed";

describe('AverageSpeed component', () => {
    it('shows survey results for each mode of transport', () => {
        const app = mount(
            <Provider store={createStore(reducers)}>
                <AverageSpeed />
            </Provider>);
        expect(app.find('.title').length).toBe(1)
        expect(app.find('.row').length).toBe(6)
        expect(app.find('.row').at(0).text()).toBe('Train: 0.57')
    })

})


