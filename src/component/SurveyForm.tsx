import React from 'react';
import {MODE_OF_TRANSPORT, Survey} from '../types'
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {Action} from "../reducers/types";
import {addSurveyResultAction} from "../reducers/actions";

const mapDispatchToProps = (dispatch: Dispatch<Action>) =>
    bindActionCreators(
        {
            addSurveyResult: addSurveyResultAction,
        },
        dispatch
    )
type SurveyFormProps = ReturnType<typeof mapDispatchToProps>

type StateProps = {
    result: {
        username: string
        modeOfTransport: string,
        distance: number
        time: number
        cost: number
    },
    isValid: boolean,
}

class SurveyForm extends React.Component<SurveyFormProps, StateProps> {

    constructor(props: any) {
        super(props)
        this.state = {
            result: {
                username: '',
                modeOfTransport: '',
                distance: 0,
                time: 0,
                cost: 0
            },
            isValid: false,
        }
        this.onClickSubmit = this.onClickSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onClickSubmit() {
        // @ts-ignore
        this.props.addSurveyResult(this.state.result)
    }

    onChange(event: React.ChangeEvent) {
        let target: HTMLInputElement | HTMLSelectElement = event.target as HTMLInputElement
        this.setState({
            result: {
                ...this.state.result,
                [target.id]: target.value
            }
        }, () => {
            if (this.state.result.username !== '' && this.state.result.modeOfTransport !== '') {
                this.setState({isValid: true})
            } else {
                this.setState({isValid: false})
            }
        })
    }

    render() {
        return (
            <div className='survey-form'>
                <p className="App-intro">
                    Name: <input id='username' value={this.state.result.username} onChange={this.onChange}></input>
                </p>
                <p className="App-intro">
                    Mode of Transport:
                    <select id="modeOfTransport" value={this.state.result.modeOfTransport} onChange={this.onChange}>
                        <option></option>
                        <option>Walk</option>
                        <option>Bike</option>
                        <option>Car</option>
                        <option>Bus</option>
                        <option>Train</option>
                        <option>Underground</option>
                    </select>
                </p>
                <p className="App-intro">
                    Distance (km): <input id="distance" type='number' value={this.state.result.distance}
                                          onChange={this.onChange}></input>
                </p>
                <p className="App-intro">
                    Time Taken (mins): <input id="time" type='number' value={this.state.result.time}
                                              onChange={this.onChange}></input>
                </p>
                <p className="App-intro">
                    Cost (£): <input id="cost" type='number' value={this.state.result.cost}
                                     onChange={this.onChange}></input>
                </p>
                {this.state.isValid && <p className="App-intro">
                  <button onClick={this.onClickSubmit}>Submit</button>
                </p>}
            </div>
        );
    }
}

export default connect(null, mapDispatchToProps)(SurveyForm);
