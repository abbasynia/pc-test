## My own Review of the code

There are a few things I would do differently.

The testing of the components which are connected to redux could be done better.
Ideally you would have a Provider and set a manual store and then check how the component changes given different stores
The mapStateToProps would include all the data manipulation as stated in the redux docs and the mapStateToProps would also have its own testing.

Would have used the StatsLayout to show all stats components a similar way.

Would have used styled-components instead of css to keep css together with JS and have a more readable flow.

Would have refactored the components to be more generic in an atoms/molecules/organisms folder structure.

Would have used selectors from 're-select' library.

Would have used Immer in the reducer, as it grows it will become more tricky to reduce the state object whilst keeping it immutable.

Would have spent more time thoroughly testing as I don't believe I've tested many of the use cases for the components and also there were no tests for the reducer which should be obligatory, but given the time constraint I treated this piece like a PoC which in 2 hours, the output on the screen is prioritised over the testing.

Would have added some linting to have a more consistent feel in coding standards.

Would have used React hooks, as it reduces the amount of noise when writing React.

From a design perspective, would have showed the results on the same page, just under the form and would have looked better seeing it live update as a user pressed submit.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
